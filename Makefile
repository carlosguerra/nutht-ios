.DEFAULT_GOAL := help
SHELL:=/bin/bash
.SILENT: init

clean: ## Runs fastlane clean
	bundle exec xcodebuild clean -workspace "NubankTakeHomeTest.xcworkspace" -scheme "NubankTakeHomeTest"

init: homebrew-install ruby-install pods-install
	echo "Project initialization is complete!"

ruby-install: ## Installs Ruby and RubyGems from Gemfile
	$(info Installing ruby, bundler and ruby gems...)
	rbenv local
	gem install bundler --no-doc -v '~> 2.0'
	bundle config set path vendor/bundle
	bundle install

homebrew-install: ## Installs homebrew and dependencies from Brewfile
	source ./bin/common/homebrew.sh && install_homebrew
	brew bundle

pods-install: ## Installs iOS project dependencies using Cocoapods
	bundle exec pod install

reset: ## Uninstalls Cocopads dependencies, RubyGems and Fastlane data
	bundle exec pod deintegrate
	bundle exec rm -rf ~/Library/Developer/Xcode/DerivedData/*
	bundle exec pod cache clean --all

help:
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' ./Makefile | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-25s\033[0m %s\n", $$1, $$2}'
