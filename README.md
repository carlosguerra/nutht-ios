# Nubank Take Home Test iOS (nuTHT iOS)

## Contents
- [Getting Started](#getting-started)
- [Running](#running)
- [Testing](#testing)
- [Releasing](#releasing)


## Getting Started
### Style Guide

For this iOS project, I follow the [Ray Wenderlich Swift Style Guide](https://github.com/raywenderlich/swift-style-guide).

 To help with the project, the primary goals are:

- Clarity of code
- Consistency throughout the codebase
- Brevity of logic/statements

All in that order.

### Prerequisites

- Make sure to have `read` and `write` access to the `nutht-iOS` repo. Please send and e-mail to [Carlos Guerra](mailto:guerrier@outlook.com?subject=[Bitbucket]%20Access%20To%20NuTHT-iOS) so I can help with that!
- Have [nutht-iOS](https://bitbucket.org/carlosguerra/nutht-ios) repo cloned to your local machine.

#### Dependencies

* [Homebrew](https://brew.sh/) - running `make homebrew-install` will only install/update Homebrew as well as all the dependencies defined in `Brewfile`, you can also run `make init` which will run through all targets needed to install/update dependencies

##### Installed through Homebrew 
* [rbenv](https://github.com/rbenv/rbenv) - running `brew install rbenv` will install `rbenv`  
* [Ruby](https://www.ruby-lang.org/en/) - running `make ruby-install` will only install `ruby` using the version defined in `.ruby-version` and the ruby packages defined in `Gemfile`, you can also run `make init` which will run through all targets needed to install/update dependencies

##### Installed using Ruby and RubyGems
* [Cocoapods](https://cocoapods.org/)


#### Setup Project - for the first time

- Run `make init` will do the following:
  - Installs:
    - `homebrew` if needed, and install `brew` dependencies listed in this project's `Brewfile` and `Brewfile.lock.json`
    - `bundler` v2.X, if needed.
    - gems
    - pods


## Running

### Locally -- How do I

#### Clean the project

- Run `make clean`.
- This helps remove pre-compiled assets that may be stale for the current build environment.
- You can also press `command + shift + K` in Xcode to clean your project.

#### Reset the project

- Close all Xcode instances 
- Run `make reset`.
- Sometimes, the projects configuration gets to a weird state and I would just like to start fresh. This command removes: the Gems, Pods, Provisioning Profiles, and build caches for Cocoapods and bundler. After running this command, you might want to run `make init` to get things setup correctly again.

#### Run project on

##### Simulator

- In Xcode, select the simulated device you would want to run on in the top center of IDE.
- Press `command + R` to run the project.

##### Physical test device

- In Xcode, select the physical device from the device dropdown, top center of IDE. Consider in this option has a development account valid and be able to run in physical devices.
- Press `command + R` to run the project.


## Testing
### Unit Tests

- In Xcode, press `command + U` to run unit tests.


## Releasing
### Pull Requests

All code merged into `master` must be merged via a pull request.
