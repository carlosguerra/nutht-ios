//
//  OriginalUrl.swift
//  NubankTakeHomeTest
//
//  Created by Carlos Guerra López on 30/03/22.
//

import Foundation
import ObjectMapper

class OriginalUrl: Mappable, CustomStringConvertible {
  var url: String = ""
  var description: String {
    return "{url: \(url)}"
  }
  
  required init?(map: Map) {}
  
  init(){}
  
  init(url: String) {
    self.url = url
  }
  
  func mapping(map: Map) {
    url <- map["url"]
  }
}
