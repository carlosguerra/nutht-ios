//
//  ShortenUrl.swift
//  NubankTakeHomeTest
//
//  Created by Carlos Guerra López on 30/03/22.
//

import Foundation
import ObjectMapper

class ShortenUrl: Mappable, CustomStringConvertible {
  var alias: String = ""
  var links: Links = Links()
  var description: String {
    return "{alias: \(alias), links: \(links.description)}"
  }
  
  required init?(map: Map) {}
  
  init() {}
  
  func mapping(map: Map) {
    alias   <- map["alias"]
    links   <- map["_links"]
  }
}
