//
//  Links.swift
//  NubankTakeHomeTest
//
//  Created by Carlos Guerra López on 30/03/22.
//

import Foundation
import ObjectMapper
import UIKit

class Links: Mappable, CustomStringConvertible {
  var original: String = ""
  var short: String = ""
  var description: String {
    return "{self: \(original), short: \(short)}"
  }
  
  required init?(map: Map) {}
  
  init(){}
  
  func mapping(map: Map) {
    original  <- map["self"]
    short     <- map["short"]
  }
}
