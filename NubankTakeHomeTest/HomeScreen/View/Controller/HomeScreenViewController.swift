//
//  HomeScreenViewController.swift
//  NubankTakeHomeTest
//
//  Created by Carlos Guerra López on 30/03/22.
//

import Foundation
import UIKit

class HomeScreenViewController: UIViewController {
  @IBOutlet weak var tableView: UITableView!
  @IBOutlet weak var urlInput: UITextField!
  
  lazy var presenter: HomeScreenPresenter = {
    return HomeScreenPresenter(view: self)
  }()
  
  override func viewDidLoad() {
    super.viewDidLoad()
    presenter.start()
  }
  
  @IBAction func shortURLOnTap(_ sender: Any) {
    presenter.shouldShortUrl()
  }
}

extension HomeScreenViewController: UITableViewDataSource {
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    presenter.getCellFor(indexPath)
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    presenter.getNumberOfAliases()
  }
}

extension HomeScreenViewController: UITableViewDelegate {
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    tableView.deselectRow(at: indexPath, animated: true)
  }
}
