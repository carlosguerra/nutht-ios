//
//  AliasGateway.swift
//  NubankTakeHomeTest
//
//  Created by Carlos Guerra López on 30/03/22.
//

import Foundation
import RxSwift

typealias AliasResultClosure = Observable<ShortenUrl>
typealias OriginalResultClosure = Observable<OriginalUrl>

protocol AliasGateway{
  func getAlias(for url: OriginalUrl) -> AliasResultClosure
  func getOriginal(for alias: String) -> OriginalResultClosure
}
