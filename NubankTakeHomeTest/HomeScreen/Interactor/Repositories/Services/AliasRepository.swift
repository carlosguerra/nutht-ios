//
//  AliasRepository.swift
//  NubankTakeHomeTest
//
//  Created by Carlos Guerra López on 30/03/22.
//

import Foundation

class AliasRepository: AliasGateway {
  func getAlias(for url: OriginalUrl) -> AliasResultClosure {
    return Server<ShortenUrl>().request(AliasAPIRouter.getShortedUrl(dataRequest: url))
  }
  
  func getOriginal(for alias: String) -> OriginalResultClosure {
    return Server<OriginalUrl>().request(AliasAPIRouter.getOriginalUrl(dataRequest: alias))
  }
}
