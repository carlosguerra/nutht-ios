//
//  AliasLocalDataSource.swift
//  NubankTakeHomeTest
//
//  Created by Carlos Guerra López on 30/03/22.
//

import Foundation

protocol AliasLocalDataSource {
  func getAlias(for index: Int) -> ShortenUrl
  func getAliases() -> [ShortenUrl]
  func setAlias(alias: ShortenUrl)
}
