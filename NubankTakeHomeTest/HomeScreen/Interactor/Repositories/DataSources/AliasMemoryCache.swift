//
//  AliasMemoryCache.swift
//  NubankTakeHomeTest
//
//  Created by Carlos Guerra López on 30/03/22.
//

import Foundation

class AliasMemoryCache: AliasLocalDataSource {
  private var aliases = [ShortenUrl]()
  
  private init() {}
  
  func getAlias(for index: Int) -> ShortenUrl {
    return aliases[index]
  }
  
  func getAliases() -> [ShortenUrl] {
    return aliases
  }
  
  func setAlias(alias: ShortenUrl) {
    aliases.append(alias)
  }
}

extension AliasMemoryCache {
  static let shared = AliasMemoryCache()
}
