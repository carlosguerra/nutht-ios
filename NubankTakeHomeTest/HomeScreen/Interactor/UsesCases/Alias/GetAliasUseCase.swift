//
//  GetAliasUseCase.swift
//  NubankTakeHomeTest
//
//  Created by Carlos Guerra López on 30/03/22.
//

import Foundation

class GetAliasUseCase: ParameteredResultUseCase {
  
  struct Input {
    let url: OriginalUrl
    
    init(url: OriginalUrl){
      self.url = url
    }
  }
  
  let gateway: AliasGateway
  
  init(gateway: AliasGateway) {
    self.gateway = gateway
  }
  
  func execute(_ input: Input) -> AliasResultClosure {
    return gateway.getAlias(for: input.url)
  }
}
