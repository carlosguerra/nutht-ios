//
//  GetOriginallUseCase.swift
//  NubankTakeHomeTest
//
//  Created by Carlos Guerra López on 30/03/22.
//

import Foundation

class GetOriginalUseCase: ParameteredResultUseCase {
  
  struct Input {
    let alias: String
    
    init(alias: String) {
      self.alias = alias
    }
  }
  
  let gateway: AliasGateway
  
  init(gateway: AliasGateway) {
    self.gateway = gateway
  }
  
  func execute(_ input: Input) -> OriginalResultClosure {
    return gateway.getOriginal(for: input.alias)
  }
}
