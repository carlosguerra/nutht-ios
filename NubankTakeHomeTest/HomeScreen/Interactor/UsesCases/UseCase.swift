//
//  UseCase.swift
//  NubankTakeHomeTest
//
//  Created by Carlos Guerra López on 30/03/22.
//

import Foundation
import RxSwift

protocol ParameteredResultUseCase {
  associatedtype Input
  associatedtype Output
  
  func execute(
    _ input: Input) -> Observable<Output>
}
