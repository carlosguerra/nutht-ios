//
//  HomeScreenWirefrime.swift
//  NubankTakeHomeTest
//
//  Created by Carlos Guerra López on 31/03/22.
//

import Foundation

class HomeScreenWirefrime {
  weak var view: HomeScreenViewController!
  
  init(view: HomeScreenViewController){
    self.view = view
  }
  
  func showActivityIndicator() {
    ActivityIndicator.sharedInstance.addActivityIndicator(to: self.view.view)
  }
  
  func removeActivityIndicator() {
    ActivityIndicator.sharedInstance.removeActivityIndicator()
  }
  
  func showMessage(for alias: ShortenUrl) {
    let controller = MessageHelper.getBasicAlert(title: "Alias", message: alias.alias)
    view.present(controller, animated: true, completion: nil)
  }
}
