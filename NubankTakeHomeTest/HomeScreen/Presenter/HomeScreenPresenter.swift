//
//  HomeScreenPresenter.swift
//  NubankTakeHomeTest
//
//  Created by Carlos Guerra López on 30/03/22.
//

import Foundation
import RxSwift

class HomeScreenPresenter {
  weak var view: HomeScreenViewController!
  private let wireframe: HomeScreenWirefrime
  private let disposeBag = DisposeBag()
  private let aliasService = AliasRepository()
  private let dataSource = AliasMemoryCache.shared
  private var aliases: [ShortenUrl]
  
  init(view: HomeScreenViewController) {
    self.view = view
    wireframe = HomeScreenWirefrime(view: view)
    aliases = dataSource.getAliases()
  }
  
  func start(){
//    view.tableView.register(UITableViewCell.self, forCellReuseIdentifier: Constants.ReuseIdentifiers.aliasCellReuseIdentifier)
  }
  
  func getCellFor(_ indexPath: IndexPath) -> UITableViewCell {
    var aliasCell = view.tableView?.dequeueReusableCell(withIdentifier: Constants.ReuseIdentifiers.aliasCellReuseIdentifier)
    if (aliasCell == nil)
    {
      aliasCell = UITableViewCell(style: UITableViewCell.CellStyle.subtitle,
                                  reuseIdentifier: Constants.ReuseIdentifiers.aliasCellReuseIdentifier)
    }
    let alias = dataSource.getAlias(for: indexPath.row)
    aliasCell!.textLabel?.text = alias.alias
    aliasCell!.detailTextLabel?.text = alias.links.short
    
    return aliasCell!
  }
  
  func shouldShortUrl() {
    let url = OriginalUrl(url: view.urlInput.text!)
    let alias = wasVisititBefore(url.url)
    if alias == nil{
      wireframe.showActivityIndicator()
      callShortURLService(for: url)
    }else {
      wireframe.showMessage(for: alias!)
    }
  }
  
  func getNumberOfAliases() -> Int {
    aliases.count
  }
  
  func wasVisititBefore(_ url: String) -> ShortenUrl?{
    for alias in aliases {
      if alias.links.original == url {
        return alias
      }
    }
    return nil
  }
  
  private func callShortURLService(for url: OriginalUrl) {
    GetAliasUseCase(gateway: aliasService).execute(GetAliasUseCase.Input(url: url)).subscribe { [weak self] shortenUrl in
      self?.wireframe.removeActivityIndicator()
      Utils.printLog("[shortenURL description] = \(shortenUrl)")
      self?.wireframe.showMessage(for: shortenUrl)
      self?.refreshTable(with: shortenUrl)
    } onError: { [weak self] error in
      self?.wireframe.removeActivityIndicator()
      Utils.printLog("[Error description] = \(error)")
    }.disposed(by: disposeBag)
  }
  
  private func refreshTable(with alias: ShortenUrl){
    dataSource.setAlias(alias: alias)
    aliases = dataSource.getAliases()
    view.tableView.reloadData()
  }
}
