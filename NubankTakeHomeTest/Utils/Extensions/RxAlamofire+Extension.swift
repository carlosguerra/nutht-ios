//
//  RxAlamofire+Extension.swift
//  NubankTakeHomeTest
//
//  Created by Carlos Guerra López on 30/03/22.
//

import Foundation
import RxSwift
import RxAlamofire
import Alamofire
import ObjectMapper

extension ObservableType where Self.Element == Alamofire.AFDataResponse<Any> {
  
  public func validateResponse() -> Observable<AFDataResponse<Any>> {
    return self.map({(dataResponse) -> AFDataResponse<Any> in
      if let response = dataResponse.response {
        if response.statusCode > HttpStatusCode.notImplemented.rawValue {
          throw ErrorEntity.notAllowedOperation()
        } else if response.statusCode > HttpStatusCode.badRequest.rawValue {
          if let json = try dataResponse.result.get() as? [String: Any],
             var errorEntity = Mapper<ErrorEntity>().map(JSONObject: json),
             json.keys.contains("AuthenticateResponse"),
             errorEntity.message != nil {
            errorEntity.status = HttpStatusCode(rawValue: response.statusCode)!
            throw errorEntity
          }
        }
      }
      return dataResponse
    })
  }
}

extension Reactive where Base: DataRequest {
  
  public func responseJSON() -> Observable<AFDataResponse<Any>> {
    return Observable.create { observer in
      let request = self.base
      
      request.responseJSON { response in
        switch response.result {
        case .success( _):
          observer.onNext(response)
          observer.onCompleted()
        case .failure( _):
          var error = ErrorEntity.notResponse()
          if response.response?.statusCode == nil {
            error = ErrorEntity.timeOut()
          } else if response.response?.statusCode == HttpStatusCode.unauthorized.rawValue {
            error = ErrorEntity.notAuthorized()
          }
          observer.onError(error)
        }
      }
      
      return Disposables.create {
        request.cancel()
      }
    }
  }
}

extension ObservableType where Element == DataRequest {
  public func responseJSON() -> Observable<AFDataResponse<Any>> {
    return flatMap { $0.rx.responseJSON() }
  }
}
