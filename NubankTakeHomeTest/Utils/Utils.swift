//
//  Utils.swift
//  NubankTakeHomeTest
//
//  Created by Carlos Guerra López on 30/03/22.
//

import Foundation

extension NSObject {
  var theClassName: String {
    return NSStringFromClass(type(of: self))
  }
}

class Utils {
  static func printLog(_ message: String, file: String = #file, function: String = #function, line: Int = #line ) {
    let className = (file.components(separatedBy: "/").last)?.components(separatedBy: ".").first
    print("[\(className!) \(function)] line: \(line) - \(message)")
  }
}
