//
//  MessageHelper.swift
//  NubankTakeHomeTest
//
//  Created by Carlos Guerra López on 31/03/22.
//
import Foundation
import UIKit

public class MessageHelper {
  typealias EmptyHandler = () -> Void
  
  static func getBasicAlert(title: String, message: String, onOk: EmptyHandler? = nil) -> UIAlertController {
    let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
    
    let OKAction = UIAlertAction(title: Constants.Localizable.actionOk.localized, style: .cancel) { (_) in
      onOk?()
    }
    alertController.addAction(OKAction)
    
    return alertController
  }
}
