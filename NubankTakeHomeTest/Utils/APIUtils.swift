//
//  APIUtils.swift
//  NubankTakeHomeTest
//
//  Created by Carlos Guerra López on 30/03/22.
//

import Foundation
import Alamofire
import ObjectMapper

class APIUtils {
  
  static func createAPIRequest(_ path: String, body: String?, httpMethod: HTTPMethod = HTTPMethod.post) -> NSMutableURLRequest {
    let url = Constants.activeIP
    var apiURL = URL(string: url)!
    apiURL = apiURL.appendingPathComponent(path)
    return createRequest(apiURL: apiURL, body: body, httpMethod: httpMethod)
  }
  
  static func createRequest(apiURL: URL, body: String?, httpMethod: HTTPMethod) -> NSMutableURLRequest {
    
    let mutableURLRequest = NSMutableURLRequest(url: apiURL)
    
    mutableURLRequest.httpMethod = httpMethod.rawValue
    mutableURLRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
    mutableURLRequest.setValue("application/json", forHTTPHeaderField: "Accept")
    
    if let dataBody = body {
      mutableURLRequest.httpBody = dataBody.data(using: String.Encoding.utf8)
    }
    
    return mutableURLRequest
  }
}
