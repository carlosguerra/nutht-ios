//
//  ActivityIndicator.swift
//  NubankTakeHomeTest
//
//  Created by Carlos Guerra López on 31/03/22.
//
import Foundation
import UIKit

class ActivityIndicator {
  
  static let sharedInstance: ActivityIndicator = {
    let instance = ActivityIndicator()
    return instance
  }()
  
  let activityIndicator: UIActivityIndicatorView
  let effectView: UIVisualEffectView
  
  private init() {
    if #available(iOS 13.0, *) {
      effectView = UIVisualEffectView(effect: UIBlurEffect(style: .systemMaterial))
      activityIndicator = UIActivityIndicatorView(style: .large)
    } else {
      effectView = UIVisualEffectView(effect: UIBlurEffect(style: .regular))
      activityIndicator = UIActivityIndicatorView(style: .white)
    }
    
    effectView.contentView.addSubview(activityIndicator)
  }
  
  func addActivityIndicator(to view: UIView) {
    effectView.frame = CGRect(x: view.frame.midX - 80.0,
                              y: view.frame.origin.y ==  0.0 ? view.frame.midY - 80.0 : view.frame.midY - 80.0 - view.frame.origin.y ,
                              width: 160,
                              height: 160)
    effectView.layer.cornerRadius = 15
    effectView.layer.masksToBounds = true
    
    activityIndicator.color = UIColor.systemIndigo
    activityIndicator.frame = CGRect(x: effectView.contentView.frame.midX - 23.0,
                                     y: effectView.contentView.frame.midY - 23.0,
                                     width: 46,
                                     height: 46)
    activityIndicator.startAnimating()
    
    view.addSubview(effectView)
  }
  
  func removeActivityIndicator() {
    activityIndicator.stopAnimating()
    effectView.removeFromSuperview()
  }
}
