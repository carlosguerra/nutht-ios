//
//  Server.swift
//  NubankTakeHomeTest
//
//  Created by Carlos Guerra López on 30/03/22.
//

import Foundation
import RxSwift
import RxAlamofire
import Alamofire
import AlamofireImage
import ObjectMapper

public final class Server<N: BaseMappable> {
  
  let manager = Session.getManager()
  let app = UIApplication.shared.delegate as? AppDelegate
  
  public func request(_ request: URLRequestConvertible) -> Observable<N> {
    let responseJson = manager.rx.request(urlRequest: request).responseJSON()
    return responseJson.validateResponse().map({ (dataResponse) -> N in
      return try self.map(data: dataResponse)
    })
  }
  
  public func arrayRequest(_ request: URLRequestConvertible, errorMessage: String? = nil) -> Observable<[N]> {
    return manager.rx.request(urlRequest: request)
      .responseJSON().validateResponse().map({ (dataResponse) -> [N] in
        return try self.arrayMap(data: dataResponse, errorMessage: errorMessage)
      })
  }
  
  public func dictionaryRequest(_ request: URLRequestConvertible) -> Observable<[String: N]> {
    return manager.rx.request(urlRequest: request)
      .responseJSON().validateResponse().map({ (dataResponse) -> [String: N] in
        return try self.dictionaryMap(data: dataResponse)
      })
  }
  
  private func errorMap(data: AFDataResponse<Any>) throws {
    if let json = data.value as? [String: Any],
       let errorEntity = Mapper<ErrorEntity>().map(JSONObject: json),
       errorEntity.message != nil {
      throw errorEntity
    }
  }
  
  private func map(data: AFDataResponse<Any>) throws -> N {
    let status = HttpStatusCode(rawValue: data.response!.statusCode)
    if status?.responseType != HttpStatusCode.ResponseType.success {
      try errorMap(data: data)
    }
    
    if let value = data.value, value is NSNull {
      let dictionary: [String: Any] = [:]
      guard let parseModel = Mapper<N>().map(JSONObject: dictionary) else {
        throw ErrorEntity.parseError()
      }
      return parseModel
    } else if N.self == EmptyResponse.self {
      return (EmptyResponse() as? N)!
    } else {
      guard let parseModel = Mapper<N>().map(JSONObject: data.value) else {
        throw ErrorEntity.parseError()
      }
      return parseModel
    }
  }
  
  private func arrayMap(data: AFDataResponse<Any>, errorMessage: String? = nil) throws -> [N] {
    try errorMap(data: data)
    guard let parseModel = Mapper<N>().mapArray(JSONObject: data.value) else {
      if let array = Mapper<EmptyResponse>().mapArray(JSONObject: data.value) {
        if array.count == 0 {
          return []
        }
      }
      
      if errorMessage != nil {
        throw ErrorEntity(message: errorMessage!, status: HttpStatusCode.internalServerError)
      }
      
      throw ErrorEntity.parseError()
    }
    return parseModel
  }
  
  private func dictionaryMap(data: AFDataResponse<Any>) throws -> [String: N] {
    try errorMap(data: data)
    guard let parseModel = Mapper<N>().mapDictionary(JSONObject: data.value) else {
      if let array = Mapper<EmptyResponse>().mapArray(JSONObject: data.value) {
        if array.count == 0 {
          return [:]
        }
      }
      throw ErrorEntity.parseError()
    }
    return parseModel
  }
}

extension Session {
  
  static func getManager() -> Session {
    
    let configuration = URLSessionConfiguration.default
    configuration.headers = .default
    configuration.timeoutIntervalForRequest = Constants.timeOut
    
    return Session(configuration: configuration)
  }
}
