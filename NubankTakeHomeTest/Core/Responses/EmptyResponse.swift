//
//  EmptyResponse.swift
//  NubankTakeHomeTest
//
//  Created by Carlos Guerra López on 30/03/22.
//

import Foundation
import ObjectMapper

class EmptyResponse: Mappable {
  
  required init?(map: Map) {
  }
  
  func mapping(map: Map) {
  }
  
  init() {
  }
}
