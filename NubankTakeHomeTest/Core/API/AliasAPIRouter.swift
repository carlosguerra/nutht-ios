//
//  AliasAPIRouter.swift
//  NubankTakeHomeTest
//
//  Created by Carlos Guerra López on 30/03/22.
//

import Alamofire
import ObjectMapper

enum AliasAPIRouter: URLRequestConvertible {
  
  private static let shortUrl       = "/api/alias"
  
  case getShortedUrl(dataRequest: OriginalUrl)
  case getOriginalUrl(dataRequest: String)
  
  public func asURLRequest() throws -> URLRequest {
    let result: (path: String, body: String?, header: String?, authenticationType: String?, httpMethod: HTTPMethod) = {
      switch  self {
      case .getShortedUrl(let dataRequest):
        return (AliasAPIRouter.shortUrl, dataRequest.toJSONString(prettyPrint: true), nil, nil, .post)
      case .getOriginalUrl(let dataRequest):
        let url = AliasAPIRouter.shortUrl + "/" + dataRequest
        return (url, nil, nil, nil, .get)
      }
    }()
    
    let request: NSMutableURLRequest = APIUtils.createAPIRequest(result.path, body: result.body, httpMethod: result.httpMethod)
    
    return request as URLRequest
  }
}
