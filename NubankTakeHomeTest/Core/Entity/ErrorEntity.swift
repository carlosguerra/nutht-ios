//
//  ErrorEntity.swift
//  NubankTakeHomeTest
//
//  Created by Carlos Guerra López on 30/03/22.
//

import Foundation
import ObjectMapper
import Localize

struct ErrorEntity: Mappable, Error, LocalizedError {
  var message: String?
  var status: HttpStatusCode = HttpStatusCode.badRequest
  
  init(message: String, status: HttpStatusCode){
    self.message = message
    self.status = status
  }
  
  init?(map: Map){
    
  }
  
  mutating func mapping(map: Map) {
    var statusValue: Int?
    
    statusValue  <- map["status"]
    message <- map["message"]
    if let statusCode = statusValue {
      status = HttpStatusCode(rawValue: statusCode)!
    }
  }
  
  static func parseError() -> ErrorEntity {
    let message = Constants.Localizable.ErrorParse.localized
    return ErrorEntity(message: message, status:HttpStatusCode.conflict)
  }
  
  static func notAuthorized() -> ErrorEntity {
    let message = Constants.Localizable.ErrorNotAuthorized.localized
    return ErrorEntity(message: message, status:HttpStatusCode.unauthorized)
  }
  
  static func notAllowedOperation() -> ErrorEntity {
    let message = Constants.Localizable.ErrorNotAllowedOperation.localized
    return ErrorEntity(message: message, status: HttpStatusCode.methodNotAllowed)
  }
  
  static func notResponse() -> ErrorEntity {
    let message = Constants.Localizable.ErrorNotAllowedOperation.localized
    return ErrorEntity(message: message, status: HttpStatusCode.noResponse)
  }
  
  static func timeOut() -> ErrorEntity {
    let message = Constants.Localizable.ErrorNotAllowedOperation.localized
    return ErrorEntity(message: message, status: HttpStatusCode.requestTimeout)
  }
  
  var description:String {
    return "{message: \(String(describing: message)), status: \(String(describing: status))}"
  }
  
  var localizedTitle:String {
    return message != nil ? message! : ""
  }
  
  var errorDescription: String? {
    get {
      var returnMessage = ""
      if let errorMessage = message {
        returnMessage = errorMessage
      }
      return returnMessage
    }
  }  
}
