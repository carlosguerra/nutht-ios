//
//  Constants.swift
//  NubankTakeHomeTest
//
//  Created by Carlos Guerra López on 30/03/22.
//

import Foundation

struct Constants {
  // MARK: - Other Constanst -
  static let activeIP = "https://url-shortener-nu.herokuapp.com"
  ///        Localizable FileName
  static let LocalizableFileName = "Localizable"
  // MARK: - Global -
  static let timeOut: Double              = 5.0
  
  // MARK: - Localizable Constants -
  struct Localizable {
    ///      General messages
    static let ErrorParse                   = "genericErrorParseResponse"
    static let ErrorNotAuthorized           = "errorNotAuthorized"
    static let ErrorNotAllowedOperation     = "genericErrorTryLater"
    ///     Splash Screen
    static let splashWelcome                = "splashWelcome"
    ///     Messages
    static let actionOk                     = "actionOk"
  }
  
  // MARK: - Segues Identifier -
  struct SeguesIdentifiers {
    static let showHome                     = "ShowHomeSegue"
  }
  
  // MARK: - Cells Identifier -
  struct ReuseIdentifiers {
    static let aliasCellReuseIdentifier              = "AliasCellReuseIdentifier"
  }
}
