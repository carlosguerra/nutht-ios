//
//  SplashViewWireframe.swift
//  NubankTakeHomeTest
//
//  Created by Carlos Guerra López on 30/03/22.
//

import Foundation

class SplashViewWireframe {
  weak var view: SplashViewController!
  
  init(view: SplashViewController!) {
    self.view = view
  }
  
  func displayHome() {
    self.view.performSegue(withIdentifier: Constants.SeguesIdentifiers.showHome, sender: self)
  }
}
