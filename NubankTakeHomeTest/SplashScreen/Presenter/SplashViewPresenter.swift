//
//  SplashViewPresenter.swift
//  NubankTakeHomeTest
//
//  Created by Carlos Guerra López on 30/03/22.
//

import Foundation

class SplashViewPresenter {
  
  weak var view: SplashViewController!
  var wireframe: SplashViewWireframe?
  
  init(view: SplashViewController!) {
    self.view = view
    wireframe = SplashViewWireframe(view: self.view)
  }
  
  func shouldDisplayHome() {
    self.wireframe?.displayHome()
  }
}
