//
//  SplashViewController.swift
//  NubankTakeHomeTest
//
//  Created by Carlos Guerra López on 30/03/22.
//

import Foundation
import UIKit

class SplashViewController: UIViewController {
  
  lazy var presenter: SplashViewPresenter = {
    return SplashViewPresenter(view: self)
  }()
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
      self.presenter.shouldDisplayHome()
    }
  }
}
